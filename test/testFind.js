const customFind = require("../find");

const items = [1,2,3,4,5,5];

function greaterThanthree( data ) {
    if(data > 3){
        return data;
    }
}
const result = customFind.find(items, greaterThanthree);
//testing 
let expOutput = 4;

if( result === expOutput) {
    console.log(result);
}