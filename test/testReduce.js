const customReduce = require('../reduce');
const items = [1,2,3,4,5,5];

function sumOfAllNumber (data, index, acc) {
    if(data !== undefined) {
        return acc+data;
    }
    
}

const result = customReduce.reduce(items, sumOfAllNumber,0);


//Testing expected output for callback function 20 when starting value are 0 or undefined(nothing)
let expOutput = 20;
if(result === expOutput) {
    console.log(result);
}
// expOutput is 25 when staring value is 5;
const result2 = customReduce.reduce(items, sumOfAllNumber,5);
let expOutput2 = 25;

if(result2 === 25) {
    console.log(result2);
}