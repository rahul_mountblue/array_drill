 const forEach = require('../each');

 const items = [1, 2, 3,4,5,5];

let newArr = [];
 function creatNewArr( data ) {
     if(data !== undefined) {
        newArr.push(data);
     }
}
 const result = forEach.each(items, creatNewArr);
//  Testing i am testing implemented foreach on two point
//  1. if return value is undefined
//  2. forEach function eradicate any null value  like [1,2,3,4,,5,5], if we print array through 
//  in built forEach its output is [1,2,3,4,5,5]

let matchValue = true;
if( result === undefined ) {
    for(let index = 0; index < items.length; index++) {
        if( newArr[index] !== items[index]) {
            matchValue = false;
        }
    }
}

if(matchValue) {
    console.log(newArr); // printing new array after push operation in custom foreach
}