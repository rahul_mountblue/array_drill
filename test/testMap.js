const customMap = require('../map');

const items = [1,2,3,4,5,5];
function square(number) {
    if(number !== undefined){
        return number*number*number;
    }
    
} 

const result = customMap.map(items, square)

//testing
// expected output [1,8,27,64,125,125]

let expArr = [1,8,27,64,125,125];
let valueMatched = true;

for(let index = 0; index < expArr.length; index++) {
    if(expArr[index] !== result[index]) {
            valueMatched = false;
    }
}

if(valueMatched) {
    console.log(result);
}