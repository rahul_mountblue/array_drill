const customFilter = require ('../filter');
const items = [1,2,3,4,5,5];

// filter all even number in array
// callback function

function evenValue(data) {
    if(data !== undefined){
        return data % 2 == 0;
    }
    
}

const result = customFilter.filter(items, evenValue);
// expected output for evenValue callback function [2,4]

let expArr = [2,4];
let matchAllValue = true;
for(let index = 0; index < expArr.length; index++) {
    if(result[index] !== expArr[index]) {
        matchAllValue = false;
    }
}

if(matchAllValue) {
    console.log(result);
}



