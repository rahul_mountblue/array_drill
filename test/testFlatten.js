const customFlat = require("../flatten");
const nestedArray = [1, [2], [[3]], [[[4]]]];
const arr = [1,[[2,3]],[4],[5]];

const result = customFlat.flatten(nestedArray);
// Testing 
let expOutput = [1,2,3,4];
let matchValue = true;

expOutput.forEach( (elem, index) => {
    if(elem !== result[index]) {
        matchValue = false;
    }
})

if(matchValue) {
    console.log(result);  //print actual output
}
