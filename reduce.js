function reduce(elements, cb, startingValue) {
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
    
    // take a starting value in accumulator
    let accumulator = startingValue === undefined ? elements[0] : startingValue;
    let indexValue;

    if(startingValue === undefined && accumulator === elements[0]) {
        indexValue = 1;
    } else {
        indexValue = 0;
    }

    for(let index = indexValue; index < elements.length; index++) {
        
            accumulator = cb(elements[index], index, accumulator);
             
   }
    return accumulator;
}
module.exports = { reduce }