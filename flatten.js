function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

    return elements.reduce((acc, cur) => {
        return Array.isArray(cur) 
            ? [...acc, ...flatten(cur)] 
            : [...acc, cur]
    }, [])
}

module.exports = { flatten }